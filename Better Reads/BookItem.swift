//
//  BookItem.swift
//  Better Reads
//
//  Created by HuyNN.local on 10/23/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit

struct BookItem: Codable {
    var bookId: String?
    var title: String
    var author: String
    var description: String
    var releaseDate: String
    var genres: [String]
    var averageRating: Double?
    var totalRatings: Int?
    var imgUrl: URL
    
    enum Keys: String, CodingKey {
        case bookId = "trackId"
        case title = "trackName"
        case author = "artistName"
        case description
        case releaseDate
        case genres
        case averageRating = "averageUserRating"
        case totalRatings = "userRatingCount"
        case imgUrl = "artworkUrl100"
    }
    
    init (from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: Keys.self)
        self.bookId = try values.decode(String.self, forKey: Keys.bookId)
        self.title = try values.decode(String.self, forKey: Keys.title)
        self.author = try values.decode(String.self, forKey: Keys.author)
        self.description = try values.decode(String.self, forKey: Keys.description)
        self.releaseDate = try values.decode(String.self, forKey: Keys.releaseDate)
        self.genres = try values.decode([String].self, forKey: Keys.genres)
        self.averageRating = try? values.decode(Double.self, forKey: Keys.averageRating)
        self.totalRatings = try? values.decode(Int.self, forKey: Keys.totalRatings)
        self.imgUrl = try values.decode(URL.self, forKey: Keys.imgUrl)
    }
}

struct BookItems: Codable {
    var results: [BookItem]
}
