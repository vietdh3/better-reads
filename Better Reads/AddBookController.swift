//
//  AddBookController.swift
//  Better Reads
//
//  Created by VietDH3 on 10/29/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit
import CoreData
class AddBookController: UITableViewController {
    
    let options = [
        "Reading Now",
        "Want to Read",
        "Have Read"
    ]
    
    var selectedIndexPath: IndexPath!
    var Item: BookItem!
    var bookId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        
        navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func save(_ sender: UIBarButtonItem) {
        //presentedViewController?.dismiss(animated: true, completion: nil)
        if bookId != nil {
            let fetchRequest: NSFetchRequest<Book> = Book.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "bookId = '\(bookId)'")
            var results = [NSManagedObject]()
            do {
                results = try Persistence.context.fetch(fetchRequest)
            } catch {
                print("Error executing fetch request: \(error)")
            }
            
            if bookId?.count == 1 {
                let objectUpdate = results[0]
                objectUpdate.setValue(option[selectedIndexPath.row], forKey: "checkMark")
            }
            
        } else {
        let book = Book(context: Persistence.context)
        book.bookId = Item.bookId
        book.author = Item.author
        book.averageRating = Item.averageRating ?? 0
        book.checkMark = options[selectedIndexPath.row]
        book.descriptionBook = Item.description
        book.genres = Item.genres.joined(separator: ",")
        book.imgUrl = Item.imgUrl
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: String(Item.releaseDate.prefix(10)))
        book.releaseDate = date! as NSDate
        book.title = Item.title
        book.totalRatings = Int16(Item.totalRatings ?? 0)
        Persistence.saveContext()
        }
        self.dismiss(animated: true, completion: nil)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AddBookController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if let index = selectedIndexPath, index == indexPath {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        cell.textLabel?.text = options[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !(navigationItem.rightBarButtonItem?.isEnabled)! {
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        selectedIndexPath = indexPath
        print(tableView.indexPathForSelectedRow?.row ?? -1)
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
}
