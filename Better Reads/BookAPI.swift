//
//  BookAPI.swift
//  Better Reads
//
//  Created by HuyNN.local on 10/20/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit


class BookAPI {
    
    public static func performSearch (with queryItems: [String:String], completion: @escaping ([BookItem]?) -> Void) {
        let baseURL = URL(string: "https://itunes.apple.com/search")
        
        guard let url = baseURL?.withQueries(queryItems) else {
            completion(nil)
            print("Unable to construct a url from the provided queries")
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data {
                do {
                    let bookItems = try JSONDecoder().decode(BookItems.self, from: data)
                    completion(bookItems.results)
                } catch let err {
                    print(err)
                }
            } else {
                print("No data was returned")
                completion(nil)
                return
            }
        }.resume()
    }
}
