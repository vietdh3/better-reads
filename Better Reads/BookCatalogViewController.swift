//
//  BookCatalogViewController.swift
//  Better Reads
//
//  Created by HuyNN.local on 10/19/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit
import  CoreData
class BookCatalogViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    let sectionTitles = ["Reading Now", "Want to Read","Have Read"]
    var s1Data = [Book]()
    var s2Data = [Book]()
    var s3Data = [Book]()

    var sectionData = [Int: [Book]]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = "My Books"
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        s1Data.removeAll()
        s2Data.removeAll()
        s3Data.removeAll()
        let fetchRequest: NSFetchRequest<Book> = Book.fetchRequest()
        do {
            let books = try Persistence.context.fetch(fetchRequest)
            print("Retrieved \(books.count) books")
            for item in books {
                print("Current book check mark: \(String(describing: item.checkMark))")
                switch item.checkMark {
                case "Reading Now"?:
                    if s1Data.count < 2 {
                        s1Data.append(item)
                    }
                case "Want to Read"?:
                    if s2Data.count < 2 {
                        s2Data.append(item)
                    }
                case "Have Read"?:
                    if s3Data.count < 2 {
                        s3Data.append(item)
                    }
                case .none:
                    print("Case none")
                case .some(_):
                    print("Case some")
                }
            }
        } catch let error {
            print("Error retrieving Core Data: \(error)")
        }
        
        collectionView.reloadData()
        print(s1Data.count)
        print(s2Data.count)
        print(s3Data.count)
        sectionData = [0: s1Data, 1: s2Data, 2: s3Data]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: "BookCell", bundle: nil), forCellWithReuseIdentifier: "BookCell")
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "show" {
                print("show my books")
                if let selectedIndexPath = collectionView.indexPathsForSelectedItems?.first {
                    let book = sectionData[selectedIndexPath.section]![(selectedIndexPath.row)]
                    let destinationVC = segue.destination as! ShowDetailBook
                    destinationVC.books = book
                }
            }
        }
    
    
}

extension BookCatalogViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sectionTitles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("s1Data: \(s1Data.count)")
        print("s2Data: \(s2Data.count)")
        print("s3Data: \(s3Data.count)")
        print("\(String(describing: sectionData[section]?.count)) item(s) in section \(section)")
        return (sectionData[section]?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookCell", for: indexPath) as! BookCell
        
        cell.configure(Data: sectionData[indexPath.section]![indexPath.row])
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderView", for: indexPath) as? HeaderView {
            sectionHeader.header.text = sectionTitles[indexPath.section]
            if sectionData[indexPath.section]?.count == 0 {
                sectionHeader.button.isEnabled = false
            }
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        let cellWidth = (collectionViewWidth - 10) / 2
        return CGSize(width: cellWidth, height: cellWidth * 0.8)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "show", sender: self)
    }
}



