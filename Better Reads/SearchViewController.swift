//
//  ViewController.swift
//  Better Reads
//
//  Created by HuyNN.local on 10/19/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit
import CoreData
class SearchViewController: UIViewController {
    

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var items = [BookItem]()
   
    // Init a SearchController, which contains the search bar
    let searchController = UISearchController(searchResultsController: nil)
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.title = "Search"
        
        // Provide collectionView with necessary data source and delegate, and register the custom cell
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: "BookCell", bundle: nil), forCellWithReuseIdentifier: "BookCell")
        
        // Configure necessary properties, including TableViewController for suggestion, and others visual effects
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        // Configure the search bar
        let searchBar = searchController.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Apple Books store"
        searchBar.delegate = self
        
        //Add the search bar to the nav bar's title
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        //Remove the title bar bottom border
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
        //end view did load
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            print("change")
            if let selectedIndexPath = collectionView.indexPathsForSelectedItems?.first {
                let book = items[selectedIndexPath.row]
                let destinationVC = segue.destination as! ShowDetailBook
                destinationVC.bookItem = book
            }
        }
        
    }
    
    
    func fetchSearchResults() {
        
        
        self.items = []
        self.collectionView.reloadData()
        
        let searchTerm = navigationItem.searchController?.searchBar.text ?? ""
        if !searchTerm.isEmpty {
            
            // set up query dictionary
            let queries = [
                "term":searchTerm,
                "media":"ebook",
                "limit":"20",
                "lang":"en_US"
            ]
            
            BookAPI.performSearch(with: queries, completion: { (bookItems) in
                DispatchQueue.main.async {
                    if let books = bookItems {
                        self.items = books
                        self.collectionView.reloadData()
                    } else {
                        print("Unable to search")
                    }
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.indicator.stopAnimating()
                }
            })
        }
    }
}

extension SearchViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookCell", for: indexPath) as! BookCell
        cell.configure(with: items[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetail", sender: self)
    }

}

extension SearchViewController: UISearchControllerDelegate, UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("Clicked \(searchBar.text!)")
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        indicator.startAnimating()
        fetchSearchResults()
    }
}

extension SearchViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let colectionViewWidth = collectionView.bounds.width
        let cellWidth = (colectionViewWidth - 10)/2
        return CGSize(width: cellWidth, height: cellWidth * 0.8)
    }
}




