//
//  ShowDetailBook.swift
//  Better Reads
//
//  Created by VietDH3 on 10/26/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit
import CoreData
class ShowDetailBook: UIViewController {
    var bookItem: BookItem!
    var books: Book!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleBook: UILabel!
    @IBOutlet weak var author: UILabel!
    
    @IBOutlet weak var addButton: UIButton!
    @IBAction func addButton(_ sender: UIButton) {
    }
    @IBOutlet weak var rate: UILabel!
    @IBOutlet weak var ratings: UILabel!
    @IBOutlet weak var descriptionBook: UILabel!
    @IBOutlet weak var releaseBook: UILabel!
    @IBOutlet weak var genreBook: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
        addButton.layer.cornerRadius = 15
        
    }
    
    func existsInCoreData(id: String) -> Bool {
        let fetchRequest: NSFetchRequest<Book> = Book.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "bookId = '\(id)'")
        
        var results = [NSManagedObject]()
        
        do {
            results = try Persistence.context.fetch(fetchRequest)
        } catch {
            print("Error executing fetch request: \(error)")
        }
        return results.count > 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let navcontroller = self.navigationController, navcontroller.viewControllers.count >= 2 {
            let viewcontroller = navcontroller.viewControllers[navcontroller.viewControllers.count - 2]
            if (viewcontroller is SearchViewController) {
                if existsInCoreData(id: bookItem.bookId!) {
                    addButton.setTitle("Update", for: .normal)
                } else {
                    addButton.setTitle("Add", for: .normal)
                }
                
            } else {
                addButton.setTitle("Update", for: .normal)
            }
        }
        if books != nil {
            
            titleBook.text = books.title
            author.text = books.author
            rate.text = "\(books.averageRating)/5"
            ratings.text = "\(books.totalRatings)"
            descriptionBook.text = books.descriptionBook
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.string(from: books.releaseDate! as Date)
            releaseBook.text = date
            genreBook.text = books.genres
            
            
            URLSession.shared.dataTask(with: books.imgUrl!) { (data, response, error) in
                if let data = data, let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self.image.image = image
                    }
                }
                }.resume()
        } else {
            
            let date = String(bookItem.releaseDate.prefix(10))
            titleBook.text = bookItem.title
            author.text = bookItem.author
            rate.text = "\(bookItem.averageRating ?? 0)/5"
            ratings.text = "\(bookItem.totalRatings ?? 0)"
            descriptionBook.text = bookItem.description
            
            releaseBook.text = date
            genreBook.text = bookItem.genres.first
            
            
            URLSession.shared.dataTask(with: bookItem.imgUrl) { (data, response, error) in
                if let data = data, let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self.image.image = image
                    }
                }
                }.resume()
        }
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationController = segue.destination as! UINavigationController
        let destination = destinationController.topViewController as! AddBookController
        destination.Item = bookItem
        
    }
}
