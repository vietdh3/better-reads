//
//  HeaderView.swift
//  Better Reads
//
//  Created by VietDH3 on 11/1/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit

class HeaderView: UICollectionReusableView {
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var button: UIButton!    

}
