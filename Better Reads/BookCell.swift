//
//  BookCell.swift
//  Better Reads
//
//  Created by HuyNN.local on 10/22/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit

class BookCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var author: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func configure(with model: BookItem) {
        title.text = model.title
        author.text = model.author
        image.image = UIImage(named: "Book")
        URLSession.shared.dataTask(with: model.imgUrl) { (data, response, error) in
            if let data = data, let cover = UIImage(data: data) {
                DispatchQueue.main.async {
                    self.image.image = cover
                }
            }
        }.resume()
    }
    
    public func configure(Data model: Book) {
        title.text = model.title
        author.text = model.author
        image.image = UIImage(named: "Book")
        URLSession.shared.dataTask(with: model.imgUrl!) { (data, response, error) in
            if let data = data, let cover = UIImage(data: data) {
                DispatchQueue.main.async {
                    self.image.image = cover
                }
            }
            }.resume()
    }
}
