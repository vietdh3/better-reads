//
//  Book+CoreDataProperties.swift
//  Better Reads
//
//  Created by VietDH3 on 11/2/18.
//  Copyright © 2018 FSoft. All rights reserved.
//
//

import Foundation
import CoreData


extension Book {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Book> {
        return NSFetchRequest<Book>(entityName: "Book")
    }

    @NSManaged public var author: String?
    @NSManaged public var averageRating: Double
    @NSManaged public var checkMark: String?
    @NSManaged public var descriptionBook: String?
    @NSManaged public var genres: String?
    @NSManaged public var imgUrl: URL?
    @NSManaged public var releaseDate: NSDate?
    @NSManaged public var title: String?
    @NSManaged public var totalRatings: Int16
    @NSManaged public var bookId: String?

}
